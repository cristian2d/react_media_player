import React, { Component } from "react";
import "./App.css";
import Preview from "./Preview";
import Playlist from "./Playlist";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Preview />
        <Playlist />
      </div>
    );
  }
}

export default App;
