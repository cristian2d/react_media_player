import React from 'react';
import { shallow } from 'enzyme';
import toJson from "enzyme-to-json";
import App from './App';

describe("<App/>", ()=>{
    it("renders",()=>{
        const wrapper = shallow(<App/>);
        expect(toJson(wrapper)).toMatchSnapshot();
    });
})



// import React from "react";
// import ReactDOM from "react-dom";
// import App from "./App";
// import configureStore from "redux-mock-store";
// import { Provider } from "react-redux";
// import { createStore, applyMiddleware } from "redux";
// import reducers from "./reducers";
// import createSagaMiddleware from "redux-saga";
// import rootSaga from "./sagas";

// const sagaMiddleware = createSagaMiddleware();

// const createStoreWithMiddleware = compose(applyMiddleware(sagaMiddleware))(
//   createStore
// );

// it("renders without crashing", () => {
//   const div = document.createElement("div");
//   ReactDOM.render(
//     <Provider store={createStoreWithMiddleware(reducers)}>
//       <App />
//     </Provider>,
//     div
//   );
//   ReactDOM.unmountComponentAtNode(div);
// });
