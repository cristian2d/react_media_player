import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./Playlist.css";
import { previewItem } from "../actions/previewActions";
import { getPlaylist } from "../selectors";
import PlaylistItem from "./PlaylistItem";

export class Playlist extends Component {
  componentDidMount() {
    const { items } = this.props;
    if (items.length > 0) {
      this.selectItem(items[0]);
    }
  }

  selectItem = item => {
    const { dispatch, currentItem } = this.props;

    if (item !== currentItem) {
      dispatch(previewItem(item));
    }
  };

  render() {
    const { items, currentItem } = this.props;
    return (
      <div>
        {items.map((item, idx) => {
          const { name, src } = item;
          return (
            <PlaylistItem
              name={name}
              selected={currentItem && currentItem.src === src}
              key={idx}
              onClick={() => {
                this.selectItem(item);
              }}
            />
          );
        })}
      </div>
    );
  }
}

Playlist.defaultProps = {
  items: []
};

const mapStateToProps = (state, props) => {
  return getPlaylist(state);
};

export default connect(mapStateToProps)(Playlist);

Playlist.propTypes = {
  items: PropTypes.array.isRequired,
  currentItem: PropTypes.object
};
