import React from "react";
import { shallow } from "enzyme";
import toJson, { renderToJson } from "enzyme-to-json";
import ConnectedPlaylist, { Playlist } from "./Playlist";

describe("<Playlist/>", () => {
  it("matches snapshot", () => {
    const item = shallow(<Playlist />);
    expect(toJson(item)).toMatchSnapshot();
  });
});
