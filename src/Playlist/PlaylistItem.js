import React, { Component } from "react";
import PropTypes from "prop-types";

export default class PlaylistItem extends Component {
  render() {
    const { name, selected, onClick } = this.props;
    return (
      <div
        className={"playlistItem" + (selected ? " selected" : "")}
        onClick={onClick}
      >
        {name}
      </div>
    );
  }
}

PlaylistItem.propType = {
  name: PropTypes.string.isRequired,
  selected: PropTypes.bool,
  onClick: PropTypes.func
};
