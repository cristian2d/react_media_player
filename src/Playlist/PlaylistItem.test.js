import React from 'react';
import { shallow, render } from 'enzyme';
import toJson, {renderToJson} from "enzyme-to-json";
import PlaylistItem from './PlaylistItem';

test("<PlaylistItem/>", ()=>{
    const title = "Title";
    const onClick = jest.fn();
    const item = shallow(<PlaylistItem name={title} onClick={onClick}/>);
    
    expect(toJson(item)).toMatchSnapshot();
    
    //Useless
    expect(item.text()).toMatch(title);

    item.simulate("click");
    expect(onClick).toBeCalled();
})