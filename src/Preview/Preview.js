import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./Preview.css";
import { getPreviewState } from "../selectors";
import { shuffle, playNext, playPrevious } from "../actions/playlistActions";

export class Preview extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playing: false,
      error: false
    };

    this.resumePlaying = false;
  }

  componentWillReceiveProps(nextProps) {
    const { item: currentItem } = this.props;
    const { item: nextItem } = nextProps;
    const { playing } = this.state;

    if (nextItem !== currentItem) {
      if (playing) {
        this.resumePlaying = true;
        this.playPause();
      }

      //it's a new item which might not fail, so we clear any error
      this.setState({
        error: false
      });
    }
  }

  playPause = () => {
    const { playing } = this.state;

    if (this.videoEl) {
      if (playing) {
        this.videoEl.pause();
      } else {
        this.videoEl.play();
      }
    }
  };

  playNext = () => {
    const { dispatch } = this.props;
    dispatch(playNext());
  };

  playPrevious = () => {
    const { dispatch } = this.props;
    dispatch(playPrevious());
  };

  onPlayerEvent = evt => {
    if (this.videoEl.error) {
      this.resumePlaying = false;
      this.videoEl.pause();
    }

    this.setState({
      error: this.videoEl.error ? true : false,
      playing: !this.videoEl.paused
    });
  };

  shuffle = () => {
    const { dispatch } = this.props;
    dispatch(shuffle());
  };

  render() {
    const { item } = this.props;
    const { playing, error } = this.state;

    return (
      <div>
        <div className="videoCnt">
          {error ? (
            <div className="cannotPlay">
              <i className="material-icons">error</i>
            </div>
          ) : (
            <video
              ref={el => {
                this.videoEl = el;
                if (el && this.resumePlaying) {
                  el.play();
                  this.resumePlaying = false;
                }
              }}
              className="videoEl"
              src={item ? item.src : ""}
              onPlaying={this.onPlayerEvent}
              onEnded={this.onPlayerEvent}
              onPlay={this.onPlayerEvent}
              onPause={this.onPlayerEvent}
              onProgress={this.onPlayerEvent}
              onError={this.onPlayerEvent}
            />
          )}
        </div>
        <div className={"previewBar" + (error ? " disabled" : "")}>
          <div>
            <i className="material-icons" onClick={this.playPrevious}>
              skip_previous
            </i>
            <i className="material-icons" onClick={this.playPause}>
              {playing ? "pause" : "play_arrow"}
            </i>
            <i className="material-icons" onClick={this.playNext}>
              skip_next
            </i>
          </div>
          <i className="material-icons shuffle" onClick={this.shuffle}>
            shuffle
          </i>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return getPreviewState(state);
};

export default connect(mapStateToProps)(Preview);

Preview.propTypes = {
  item: PropTypes.object,
  state: PropTypes.oneOf(["paused", "playing"])
};
