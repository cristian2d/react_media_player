import React from "react";
import { shallow } from "enzyme";
import toJson, { renderToJson } from "enzyme-to-json";
import ConnectedPreview, { Preview } from "./Preview";

describe("<Preview/>", () => {
  it("matches snapshot", () => {
    const item = shallow(<Preview />);
    expect(toJson(item)).toMatchSnapshot();
  });

  it("matches playing snapshot", () => {
    const item = shallow(<Preview />);
    item.setState({
      playing: true
    });
    expect(toJson(item)).toMatchSnapshot();
  });

  it("matches error snapshot", () => {
    const item = shallow(<Preview />);
    item.setState({
      error: true
    });
    expect(toJson(item)).toMatchSnapshot();
  });
});
