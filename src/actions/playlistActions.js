export const PLAY_NEXT = "PLAY_NEXT";
export const PLAY_PREVIOUS = "PLAY_PREVIOUS";
export const SHUFFLE = "SHUFFLE";

export const playNext = () => {
  return {
    type: PLAY_NEXT
  };
};

export const playPrevious = () => {
  return {
    type: PLAY_PREVIOUS
  };
};

export const shuffle = () => {
  return {
    type: SHUFFLE
  };
};
