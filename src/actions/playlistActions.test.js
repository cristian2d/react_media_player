import {
  PLAY_NEXT,
  PLAY_PREVIOUS,
  SHUFFLE,
  playNext,
  playPrevious,
  shuffle
} from "./playlistActions";

describe(">>>ACTION -- playlist actions", () => {
  it("+++ actionCreator playNext", () => {
    const action = playNext();
    expect(action).toEqual({
      type: PLAY_NEXT
    });
  });

  it("+++ actionCreator playPrevious", () => {
    const action = playPrevious();
    expect(action).toEqual({
      type: PLAY_PREVIOUS
    });
  });

  it("+++ actionCreator shuffle", () => {
    const action = shuffle();
    expect(action).toEqual({
      type: SHUFFLE
    });
  });
});
