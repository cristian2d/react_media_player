export const PREVIEW_ITEM = "PREVIEW_ITEM";

export const previewItem = item => {
  return {
    type: PREVIEW_ITEM,
    payload: item
  };
};
