import { PREVIEW_ITEM, previewItem } from "./previewActions";

describe(">>>ACTION -- preview actions", () => {
  it("+++ actionCreator previewItem", () => {
    const item = {};
    const action = previewItem(item);
    expect(action).toEqual({
      type: PREVIEW_ITEM,
      payload: item
    });
  });
});
