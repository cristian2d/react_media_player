import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import reducers from "./reducers";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas";
import { shallow, mount } from "enzyme";

const sagaMiddleware = createSagaMiddleware();

const createStoreWithMiddleware = compose(applyMiddleware(sagaMiddleware))(
  createStore
);

const store = createStoreWithMiddleware(reducers);

it("renders without crashing", () => {
  const wrapper = mount(
    <Provider store={store}>
      <App />
    </Provider>
  );
  console.log(wrapper);
});
