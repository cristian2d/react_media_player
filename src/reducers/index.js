import { combineReducers } from "redux";
import playlistReducer from "./playlist";
import previewReducer from "./preview";

export default combineReducers({
  playlist: playlistReducer,
  preview: previewReducer
});
