import { createStore } from "redux";
import rootReducer from "./index";
import playlistReducer from "./playlist";
import previewReducer from "./preview";

describe(">>>R E D U C E R --- ROOT reducer", () => {
  const store = createStore(rootReducer);
  test("initial state matches child reducers ", () => {
    expect(store.getState().playlist).toEqual(playlistReducer(undefined, {}));
    expect(store.getState().preview).toEqual(previewReducer(undefined, {}));
  });
});
