import { getDummyItems } from "./utils";
import { SHUFFLE } from "../actions/playlistActions";

const defaultState = {
  items: getDummyItems()
};

const playlistReducer = (state = defaultState, { type, payload }) => {
  switch (type) {
    case SHUFFLE:
      return produceNextShuffle(state);
    default:
      return state;
  }
};

export default playlistReducer;

const produceNextShuffle = state => {
  let newItems = [...state.items];
  shuffleArray(newItems);
  return {
    ...state,
    items: newItems
  };
};

/**
 * https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
 * @param {*} array
 */
const shuffleArray = array => {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
};
