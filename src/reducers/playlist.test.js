import playlistReducer from "./playlist";
import { createItemFromLocalPath, getDummyItems } from "./utils";
import { SHUFFLE } from "../actions/playlistActions";

describe(">>>R E D U C E R --- playlist reducer", () => {
  it("+++ reducer on defaultState", () => {
    const initialState = {
      items: getDummyItems()
    };
    const state = playlistReducer(undefined, {});
    expect(state).toEqual(initialState);
  });

  it("+++ reducer for SHUFFLE", () => {
    const initialState = {
      items: ["1", 2, "3"].map(path => {
        return createItemFromLocalPath(path);
      })
    };
    const state = playlistReducer(initialState, { type: SHUFFLE });
    //we check if the items still exist in the shuffled array
    for (let i = 0; i < initialState.items.length; i++) {
      expect(state.items.indexOf(initialState.items[i])).toBeGreaterThan(-1);
    }
  });
});
