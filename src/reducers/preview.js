import { PREVIEW_ITEM } from "../actions/previewActions";
const defaultState = { item: null };

const previewReducer = (state = defaultState, { type, payload }) => {
  switch (type) {
    case PREVIEW_ITEM:
      return produceNextPreviewItem(state, payload);
    default:
      return state;
  }
};

export default previewReducer;

const produceNextPreviewItem = (state, item)=>{
  return {
    ...state,
    item: item
  }
};
