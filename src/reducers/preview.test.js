import previewReducer from "./preview";
import { PREVIEW_ITEM } from "../actions/previewActions";
import { createItemFromLocalPath } from "./utils";

describe(">>>R E D U C E R --- preview reducer", () => {
  it("+++ reducer on defaultState", () => {
    const initialState = { item: null };
    const state = previewReducer(undefined, {});
    expect(state).toEqual(initialState);
  });

  it("+++ reducer for PREVIEW_ITEM", () => {
    const initialState = {
      item: null
    };
    const itemToAdd = createItemFromLocalPath("1");
    const state = previewReducer(initialState, {
      type: PREVIEW_ITEM,
      payload: itemToAdd
    });
    expect(state).toEqual({
      item: itemToAdd
    });
  });
});
