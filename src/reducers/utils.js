import { last } from "ramda";

const defaultMediaNames = [
    "1_avc_360p.mp4",
    "1_avc_1080p.mp4",
    "1_avc_uhd.mp4",
    "1_vp9_360p.webm",
    "1_vp9_1080p.webm",
    "2_avc_360p.mp4",
    "2_avc_1080p.mp4",
    "2_avc_uhd.mp4",
    "2_vp9_360p.webm",
    "2_vp9_1080p.webm",
    "2_vp9_uhd.webm",
    "this_one_fails"
  ],
  defaultMediaPath = "/defaultMedia/";

const nameToPath = name => {
  if (name) {
    return last(String(name).split("/"));
  }

  return name;
};

export const createItemFromLocalPath = (path = "") => {
  return {
    src: path,
    name: nameToPath(path)
  };
};

export const getDummyItems = () => {
  return defaultMediaNames.map(name => {
    return createItemFromLocalPath(defaultMediaPath + name);
  });
};
