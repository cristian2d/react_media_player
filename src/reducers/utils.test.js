import { createItemFromLocalPath, getDummyItems } from "./utils";

describe(">>>R E D U C E R --- utils", () => {
  it("+++ creates dummy items", () => {
    const items = getDummyItems();
    expect(items.length).toBeGreaterThan(1);
  });

  it("+++ creates empty item", () => {
    const item = createItemFromLocalPath(undefined);
    expect(item).toEqual({
      name: "",
      src: ""
    });
  });
});
