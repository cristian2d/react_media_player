import { all, put, takeEvery, takeLatest, select } from "redux-saga/effects";
import { PLAY_NEXT, PLAY_PREVIOUS } from "../actions/playlistActions";
import { previewItem } from "../actions/previewActions";
import { getPlaylist } from "../selectors";

export function* playNext() {
  const { currentItem, items } = yield select(getPlaylist);
  let idx = yield items.indexOf(currentItem);
  let nextItem = yield idx < items.length - 1 ? items[idx + 1] : items[0];
  yield put(previewItem(nextItem));
}

export function* playPrevious() {
  const { currentItem, items } = yield select(getPlaylist);
  let idx = yield items.indexOf(currentItem);
  let nextItem = yield idx > 0 ? items[idx - 1] : items[items.length - 1];
  yield put(previewItem(nextItem));
}

export default function* rootSaga() {
  yield all([
    takeLatest(PLAY_NEXT, playNext),
    takeEvery(PLAY_PREVIOUS, playPrevious)
  ]);
}
