import { testSaga, expectSaga } from "redux-saga-test-plan";
import { runSaga } from "redux-saga";
import {
  all,
  put,
  takeEvery,
  takeLatest,
  select,
  take,
  call
} from "redux-saga/effects";
import { PLAY_NEXT, PLAY_PREVIOUS } from "../actions/playlistActions";
import { previewItem } from "../actions/previewActions";
import { getPlaylist } from "../selectors";
import rootSaga, { playNext, playPrevious } from "./index";

describe(">>>SAGA -- sagas", () => {
  const item0 = { name: "0" },
    item1 = { name: "1" },
    item2 = { name: "2" };
  const dispatched = [];
  const state = {
    preview: { item: item1 },
    playlist: {
      items: [item0, item1, item2]
    }
  };

  //   it("plays next", () => {
  //     return expectSaga(playNext)
  //       .withState(state)
  //       .run()
  //       .then(result => {
  //         console.log(result);
  //       });
  //   });

  //   it("plays previous", () => {
  //     return expectSaga(playPrevious)
  //       .withState(state)
  //       .run();
  //   });

  it("plays next", () => {
    // const generator = playNext();
    // let next = generator.next();
    // expect(next.value).toEqual(select(getPlaylist));
    // next = generator.next(getPlaylist(state));
    // expect(next.value).toEqual(0);
    // next = generator.next(0);
    // expect(next.value).toEqual(item);
    // next = generator.next(item);
    // console.log(next)
    // console.log(next.value)
    // expect(next.value).toEqual(put(previewItem(item)));
    // next = generator.next();
    // expect(next).toEqual({
    //   done: true,
    //   value: undefined
    // });
    testSaga(playNext)
      .next()
      .select(getPlaylist)
      .next(getPlaylist(state))
      .is(1)
      .next(1)
      .is(item2)
      .next(item2)
      .put(previewItem(item2))
      .next()
      .isDone();
  });

  it("plays next being last", () => {
    testSaga(playNext)
      .next()
      .select(getPlaylist)
      .next(
        getPlaylist({
          preview: { item: item2 },
          playlist: {
            items: [item0, item1, item2]
          }
        })
      )
      .is(2)
      .next(2)
      .is(item0)
      .next(item0)
      .put(previewItem(item0))
      .next()
      .isDone();
  });

  it("plays previous", () => {
    testSaga(playPrevious)
      .next()
      .select(getPlaylist)
      .next(getPlaylist(state))
      .is(1)
      .next(1)
      .is(item0)
      .next(item0)
      .put(previewItem(item0))
      .next()
      .isDone();
  });

  it("plays previous being first", () => {
    testSaga(playPrevious)
      .next()
      .select(getPlaylist)
      .next(
        getPlaylist({
          preview: { item: item0 },
          playlist: {
            items: [item0, item1, item2]
          }
        })
      )
      .is(0)
      .next(0)
      .is(item2)
      .next(item2)
      .put(previewItem(item2))
      .next()
      .isDone();
  });

  it("listens to actions", () => {
    testSaga(rootSaga)
      .next()
      .all([
        takeLatest(PLAY_NEXT, playNext),
        takeEvery(PLAY_PREVIOUS, playPrevious)
      ])
      .next()
      .isDone();
  });

  it("snapshot playNext", () => {
    return expectSaga(playNext)
      .withState(state)
      .run();
  });
});
