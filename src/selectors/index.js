import { createSelector } from "reselect";

const getPreviewingItem = state => state.preview.item;
const getItems = state => state.playlist.items;

export const getPlaylist = createSelector(
  [getPreviewingItem, getItems],
  (item, items) => {
    return {
      currentItem: item,
      items: items
    };
  }
);

export const getPreviewState = state => state.preview;
