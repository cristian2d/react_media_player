import { createItemFromLocalPath } from "../reducers/utils";
import { getPlaylist, getPreviewState } from "./index";

const mockCreator = items => {
  const mockedPlaylistState = {
    items: items.map(path => {
      return createItemFromLocalPath(path);
    })
  };

  const dummyItem = mockedPlaylistState.items[0];
  const mockedPreviewState = { item: dummyItem };
  const mockedState = {
    preview: mockedPreviewState,
    playlist: mockedPlaylistState
  };

  return { mockedState, mockedPreviewState, mockedPlaylistState, dummyItem };
};

describe(">>>SELECTOR -- selectors", () => {
  const {
    mockedState,
    mockedPreviewState,
    mockedPlaylistState,
    dummyItem
  } = mockCreator(["1", 2]);
  it("+++ selector getPreviewState", () => {
    const result = getPreviewState(mockedState);
    expect(result).toEqual(mockedPreviewState);
  });
  it("+++ selector getPlaylist", () => {
    const result = getPlaylist(mockedState);
    expect(result).toEqual({
      currentItem: dummyItem,
      items: mockedPlaylistState.items
    });
  });
});
